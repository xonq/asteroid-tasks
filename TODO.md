# 2021/01/04

- <s> try removing excess `.qml`, simplifying scripts </s>

- <s> generate generic binary goal </s>

- <s> generate generic float goal </s>

- generate add/rm function

- create icons selection page (based on wallpaper) `/usr/share/icons/asteroid/`

- <s> dynamic main menu generation of list items </s>

- <s> generate date change functionality </s>

- categories

- add categories from template if not in selected date

- create a page to overview for the week

- appropriately order main menu
